from flask import (Flask,
                   jsonify,
                   abort,
                   request, url_for, send_from_directory)
import mysql.connector

from mysql.connector import Error
from mysql.connector.connection import MySQLConnection
from mysql.connector import pooling

# from connection import *
from flask_cors import cross_origin
import logging, os
from werkzeug.utils import secure_filename

BASE_URI = '/api'

USERS = '/users'
USER = '/user'

GET_LOGIN = '/login'

UPLOAD = '/upload'

TASKS = '/tasks'
TASK = '/task'

FILE = '/file'

ASSIGN = '/assign'

UNASSIGN = '/unassign'

PERCENT = '/percent'

COMPLETE = '/complete'

STATUS = '/status'

ASSIGNED = '/assigned'

# URI LIST
uri_all_users = BASE_URI + USERS
uri_specific_user = BASE_URI + USER
uri_get_login = BASE_URI + GET_LOGIN
uri_upload = BASE_URI + UPLOAD
uri_all_tasks = BASE_URI + TASKS
uri_specific_task = BASE_URI + TASK
uri_specific_task_file = BASE_URI + TASK + FILE
uri_specific_users_tasks = BASE_URI + USER + TASKS
uri_assign_task = BASE_URI + TASK + ASSIGN
uri_unassign_task = BASE_URI + TASK + UNASSIGN
uri_user_comp_percent = BASE_URI + USER + TASKS + PERCENT
uri_get_task_status = BASE_URI + USER + TASK + STATUS
uri_update_task_status = BASE_URI + USER + TASK + STATUS + COMPLETE
uri_get_all_assignments = BASE_URI + TASKS + ASSIGNED

uri_get_login_pass = BASE_URI + GET_LOGIN +"2"

PROJECT_HOME = os.path.dirname(os.path.realpath(__file__))
UPLOAD_FOLDER = '{}/uploads/'.format(PROJECT_HOME)

STATIC_FILE_PATH = "http://localhost:5000/uploads/"


connection_pool = mysql.connector.pooling.MySQLConnectionPool(pool_name="db_pool",
                                                                  pool_size=32,
                                                                  pool_reset_session=True,
                                                                  host='localhost',
                                                                  database='trainingday_db',
                                                                  user='api_usr',
                                                                  password='KgktDyjuv23WUf3G')

print ("Printing connection pool properties ")
print("Connection Pool Name - ", connection_pool.pool_name)
print("Connection Pool Size - ", connection_pool.pool_size)

# Get connection object from a pool
connection_object_start = connection_pool.get_connection()

if connection_object_start.is_connected():
    db_Info = connection_object_start.get_server_info()
    print("Connected to MySQL database using connection pool ... MySQL Server version on ",db_Info)


def create_new_folder(local_dir):
    newpath = local_dir
    if not os.path.exists(newpath):
        os.makedirs(newpath)
    return newpath


app = Flask(__name__)


@app.route('/uploads/<path:path>')
def send_file(path):
    return send_from_directory('uploads', path)


# GET ALL USERS
@app.route(uri_all_users, methods=['GET'])
@cross_origin()
def users():
    query = 'SELECT * FROM users'
    connection_object = connection_pool.get_connection()

    cursor = connection_object .cursor()
    cursor.execute(query)
    rows = cursor.fetchall()

    items = []

    for row in rows:
        dict = {}
        for (key, value) in zip(cursor.description, row):
            dict[key[0]] = value

        items.append(dict)
        print(dict)

    cursor.close()
    connection_object.commit()
    connection_object.close()

    return jsonify(items), 200


#Get specific user
@app.route(uri_specific_user + '/<int:ID>', methods=["GET"])
@cross_origin()
def get_user(ID):

    connection_object = connection_pool.get_connection()
    cursor = connection_object.cursor()

    query = "SELECT `ID`,`password`,`accesslevel` FROM `users` WHERE `ID` = "+str(ID)

    # execute the query
    cursor.execute(query, (ID))
    # fetch the row matching the id
    row = cursor.fetchone()
    # fetch the column names
    columns = cursor.column_names
    # declare an empty dictionary for the table data
    tableData = {}
    for key, value in zip(columns, row):
        tableData[key] = value

    print(row)

    # close the connection to the database
    cursor.close()
    connection_object.commit()
    connection_object.close()

    # return the dictionary to the handler
    return jsonify(tableData)


# ADD USER
@app.route(uri_all_users, methods=['POST'])
@cross_origin()
def add_user():
    if not request.json:
        print(request.get_json())
        abort(400)

    connection_object = connection_pool.get_connection()

    cursor = connection_object.cursor()

    # query = "INSERT IGNORE INTO users (username, password) values (%s, MD5(%s))"
    query = "INSERT INTO users (username, password) SELECT %s, MD5(%s) FROM DUAL WHERE NOT EXISTS( SELECT 1 FROM users WHERE username = %s ) LIMIT 1"

    cursor.execute(query, (request.json['username'],
                           request.json['password'], request.json['username']))

    id = cursor.lastrowid

    query = "SELECT * FROM users WHERE ID=" + str(id)
    cursor.execute(query)
    row = cursor.fetchone()

    staff = {}
    for (key, value) in zip(cursor.description, row):
        staff[key[0]] = value

    cursor.close()
    connection_object.commit()
    connection_object.close()

    return jsonify(staff), 201


# GET PASS LOGIN
@app.route(uri_get_login + '/<string:username>', methods=["GET"])
@cross_origin()
# function to get specific item by their ID
def get_login(username):
    # temporary connection to local database during development - cant connect to scm
    # TODO: update once able to connect to scm

    connection_object = connection_pool.get_connection()
    cursor = connection_object.cursor()


    query = "SELECT `ID`,`password`,`accesslevel` FROM `users` WHERE `username` = '%s'" % (str(username))
    print(query)

    # execute the query
    cursor.execute(query)
    # fetch the row matching the id
    row = cursor.fetchone()
    # fetch the column names
    columns = cursor.column_names
    # declare an empty dictionary for the table data
    tableData = {}
    for key, value in zip(columns, row):
        tableData[key] = value

    print(row)

    # close the connection to the database
    cursor.close()
    connection_object.commit()
    connection_object.close()

    # return the dictionary to the handler
    return jsonify(tableData)


#GET USER LOGIN
@app.route(uri_get_login_pass + '/<string:username>', methods=["GET", "POST"])
@cross_origin()
# function to get specific item by their ID
def get_login_2(username):
    if not request.json:
        print(request.get_json())
        abort(400)
    else:
        print(request.get_json())

    password = request.json['password']
    connection_object = connection_pool.get_connection()

    cursor = connection_object.cursor(buffered=True)

    connection_object = connection_pool.get_connection()
    cursor = connection_object.cursor()


    query = "SELECT `ID`,`accesslevel`,`username` FROM `users` WHERE `username` = '%s' AND password = MD5('%s')" % (str(username), str(password))
    print(query)

    # execute the query
    cursor.execute(query)
    # fetch the row matching the id
    rows = cursor.fetchall()

    items = []


    for row in rows:
        dict = {}
        for (key, value) in zip(cursor.description, row):
            dict[key[0]] = value

        items.append(dict)
        print(dict)

    print(len(items))

    if len(items) == 0:
        print("Failed login")
        cursor.close()
        connection_object.commit()
        connection_object.close()
        return ""
    else:
        cursor.close()
        connection_object.commit()
        connection_object.close()
        return jsonify(items), 200
    return


#UPLOAD FILE
@app.route(uri_upload, methods=['GET', 'POST'])
@cross_origin()
def upload():
    app.logger.info(PROJECT_HOME)

    if request.files['image']:
        # print(app.config['UPLOAD_FOLDER'])
        img = request.files['image']
        img_name = secure_filename(img.filename)
        create_new_folder(UPLOAD_FOLDER)
        saved_path = os.path.join(UPLOAD_FOLDER, img_name)
        # print("saving {}".format(saved_path))
        img.save(saved_path)

        # END FILE

        connection_object = connection_pool.get_connection()

        TASK_NAME = str(request.form.get("task_name"))
        TASK_DESC = str(request.form.get("task_desc"))

        FILE_NAME = img_name
        FILE_PATH = STATIC_FILE_PATH+FILE_NAME

        print("TASK DETAILS: " + TASK_NAME +"   "+TASK_DESC)

        #TASKS DONE BEGIN UPLOADS

        cursor = connection_object .cursor()

        query = "INSERT INTO uploads (file_name, file_path) values (%s, %s)"

        cursor.execute(query,
                       [FILE_NAME,
                        FILE_PATH])


        id = cursor.lastrowid

        query = "SELECT * FROM uploads WHERE ID=" + str(id)
        cursor.execute(query)
        row = cursor.fetchone()

        # uploads = {}
        # for (key, value) in zip(cursor.description, row):
        #     uploads[key[0]] = value
        #
        # print(jsonify(uploads))

        print("UPLOAD ID: "+str(row[0]))
        # BEGIN TASK DB INSERT
        UPLOAD_ID = str(row[0])



        cursor = connection_object .cursor()

        query = "INSERT INTO tasks (task_name, task_desc, UID) values (%s, %s, %s)"

        print("QUERY:   "+query)

        cursor.execute(query,
                       [TASK_NAME,
                        TASK_DESC,
                        UPLOAD_ID])

        id = cursor.lastrowid


        query = "SELECT * FROM tasks WHERE ID=" + str(id)
        cursor.execute(query)
        row = cursor.fetchone()

        # tasks = {}
        # for (key, value) in zip(cursor.description, row):
        #     tasks[key[0]] = value

        print("TASK ID: "+str(row[0]))

        cursor.close()
        connection_object.commit()
        connection_object.close()


        return send_from_directory(UPLOAD_FOLDER, img_name, as_attachment=True)
    else:
        return "Where is the image?"


# GET ALL TASKS
@app.route(uri_all_tasks, methods=['GET'])
@cross_origin()
def tasks():
    query = 'SELECT * FROM tasks'
    connection_object = connection_pool.get_connection()

    cursor = connection_object .cursor()
    cursor.execute(query)
    rows = cursor.fetchall()

    items = []

    for row in rows:
        dict = {}
        for (key, value) in zip(cursor.description, row):
            dict[key[0]] = value

        items.append(dict)
        print(dict)

    cursor.close()
    connection_object.commit()
    connection_object.close()


    return jsonify(items), 200


#GET SPECIFIC USERS TASK
@app.route(uri_specific_users_tasks + '/<int:ID>', methods=["GET"])
@cross_origin()
def get_users_tasks(ID):

    connection_object = connection_pool.get_connection()
    cursor = connection_object.cursor()

    query = "SELECT tasks.ID, tasks.task_name, tasks.task_desc, assigned_tasks.complete FROM assigned_tasks LEFT JOIN tasks ON tasks.ID = assigned_tasks.TID LEFT JOIN users ON users.ID = assigned_tasks.USRID WHERE assigned_tasks.USRID ="+str(ID)
    print(query)

    # execute the query
    cursor.execute(query)
    # fetch the row matching the id
    rows = cursor.fetchall()

    items = []

    for row in rows:
        dict = {}
        for (key, value) in zip(cursor.description, row):
            dict[key[0]] = value

        items.append(dict)
        print(dict)

    cursor.close()
    connection_object.commit()
    connection_object.close()

    return jsonify(items), 200


#GET SPECIFIC TASK
@app.route(uri_specific_task + '/<int:ID>', methods=["GET"])
@cross_origin()
def get_task(ID):

    connection_object = connection_pool.get_connection()
    cursor = connection_object.cursor()

    query = "SELECT * FROM `tasks` WHERE `ID` = "+str(ID)
    print(query)

    # execute the query
    cursor.execute(query)
    # fetch the row matching the id
    row = cursor.fetchone()
    # fetch the column names
    columns = cursor.column_names
    # declare an empty dictionary for the table data
    tableData = {}
    for key, value in zip(columns, row):
        tableData[key] = value

    print(row)

    # close the connection to the database
    cursor.close()
    connection_object.commit()
    connection_object.close()

    # return the dictionary to the handler
    return jsonify(tableData)


#GET FILE FOR TASK
@app.route(uri_specific_task_file + '/<int:ID>', methods=["GET"])
@cross_origin()
def get_task_file(ID):

    connection_object = connection_pool.get_connection()
    cursor = connection_object.cursor()

    query = "SELECT tasks.ID, tasks.task_name, uploads.file_path FROM tasks INNER JOIN uploads ON tasks.UID = uploads.ID WHERE tasks.ID ="+str(ID)
    print(query)

    # execute the query
    cursor.execute(query, (ID))
    # fetch the row matching the id
    row = cursor.fetchone()
    # fetch the column names
    columns = cursor.column_names
    # declare an empty dictionary for the table data
    tableData = {}
    for key, value in zip(columns, row):
        tableData[key] = value

    print(row)

    # close the connection to the database
    cursor.close()
    connection_object.commit()
    connection_object.close()

    # return the dictionary to the handler
    return jsonify(tableData)


#ASSIGN TASK TO USER
@app.route(uri_assign_task, methods=['POST'])
@cross_origin()
def assign_task():
    if not request.json:
        print(request.get_json())
        abort(400)
    else:
        print(request.get_json())

    connection_object = connection_pool.get_connection()

    cursor = connection_object.cursor()

    TID = int(request.json['TID'])
    USRID = int(request.json['USRID'])



    # query = "INSERT INTO users (username, password) values (%s, %s)"
    query = "INSERT INTO assigned_tasks (TID, USRID) values (%s, %s)"



    print(query +"  "+str(TID) + "   "+str(USRID))

    cursor.execute(query, (TID,
                           USRID))


    id = cursor.lastrowid

    cursor.close()
    connection_object.commit()
    connection_object.close()

    return "Assigned"


#UN-ASSIGN TASK FROM USER
@app.route(uri_unassign_task, methods=['POST'])
@cross_origin()
def unassign_task():
    if not request.json:
        print(request.get_json())
        abort(400)
    else:
        print(request.get_json())

    connection_object = connection_pool.get_connection()

    cursor = connection_object.cursor()

    TNM = request.json['TNM']
    USRNM = request.json['USRNM']



    # query = "INSERT INTO users (username, password) values (%s, %s)"
    query = "DELETE assigned_tasks FROM assigned_tasks JOIN tasks ON assigned_tasks.TID = tasks.ID JOIN users ON assigned_tasks.USRID = users.ID WHERE tasks.task_name=%s AND users.username=%s"



    print(query +"  "+str(TNM) + "   "+str(USRNM))

    cursor.execute(query, (TNM,
                           USRNM))


    id = cursor.lastrowid

    cursor.close()
    connection_object.commit()
    connection_object.close()

    return "Un-Assigned"


#GET TASK STATUS
@app.route(uri_get_task_status, methods=['POST'])
@cross_origin()
def get_task_status():
    if not request.json:
        print(request.get_json())
        abort(400)
    else:
        print(request.get_json())

    connection_object = connection_pool.get_connection()

    cursor = connection_object.cursor(buffered=True)

    TID = request.json['TID']
    USRID = request.json['USRID']



    # query = "INSERT INTO users (username, password) values (%s, %s)"
    query = "SELECT assigned_tasks.ID, assigned_tasks.complete FROM assigned_tasks INNER JOIN users ON assigned_tasks.USRID = users.ID WHERE users.ID = %s AND assigned_tasks.TID = %s"



    print(query +"  "+str(TID) + "   "+str(USRID))

    cursor.execute(query, (USRID,
                           TID))

    row = cursor.fetchone()
    # fetch the column names
    columns = cursor.column_names
    # declare an empty dictionary for the table data
    tableData = {}
    for key, value in zip(columns, row):
        tableData[key] = str(value)

    print(row)

    # close the connection to the database
    cursor.close()
    connection_object.commit()
    connection_object.close()

    # return the dictionary to the handler
    return jsonify(tableData)


#UPDATE TASK STATUS
@app.route(uri_update_task_status, methods=['POST'])
@cross_origin()
def update_task_status():
    if not request.json:
        print(request.get_json())
        abort(400)
    else:
        print(request.get_json())

    connection_object = connection_pool.get_connection()

    cursor = connection_object.cursor()

    TID = request.json['TID']
    USRID = request.json['USRID']


    query = "UPDATE `assigned_tasks` SET `complete`='true' WHERE`USRID` = %s AND `TID` = %s"



    print(query +"  "+str(TID) + "   "+str(USRID))

    cursor.execute(query, (USRID,
                           TID))

    id = cursor.lastrowid

    cursor.close()
    connection_object.commit()
    connection_object.close()

    return "Task Completed"


#GET PERCENTAGE OF TASKS COMPLETE FOR USER
@app.route(uri_user_comp_percent+ '/<int:ID>', methods=["GET"])
@cross_origin()
def get_user_comp_percent(ID):

    connection_object = connection_pool.get_connection()
    cursor = connection_object.cursor()

    query = "select USRID, users.username, sum(case when complete <> 'true' then 1 else 0 end) * 100 / count(*) as 'percent' from assigned_tasks INNER JOIN users ON assigned_tasks.USRID = users.ID WHERE users.ID ="+str(ID)
    print(query)

    # execute the query
    cursor.execute(query, (ID))
    # fetch the row matching the id
    row = cursor.fetchone()
    # fetch the column names
    columns = cursor.column_names
    # declare an empty dictionary for the table data
    tableData = {}
    for key, value in zip(columns, row):
        tableData[key] = str(value)

    print(row)

    # close the connection to the database
    cursor.close()
    connection_object.commit()
    connection_object.close()

    # return the dictionary to the handler
    return jsonify(tableData)


#GET ALL ASSIGNMENTS - ADMIN
@app.route(uri_get_all_assignments, methods=["GET"])
@cross_origin()
def get_tasks_assigned():
    query = 'SELECT assigned_tasks.ID, users.username, tasks.task_name, assigned_tasks.complete FROM `assigned_tasks` INNER JOIN tasks ON assigned_tasks.TID = tasks.ID INNER JOIN users ON assigned_tasks.USRID = users.ID'
    connection_object = connection_pool.get_connection()

    cursor = connection_object .cursor()
    cursor.execute(query)
    rows = cursor.fetchall()

    items = []

    for row in rows:
        dict = {}
        for (key, value) in zip(cursor.description, row):
            dict[key[0]] = value

        items.append(dict)
        print(dict)

    cursor.close()
    connection_object.commit()
    connection_object.close()

    return jsonify(items), 200
